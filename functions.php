<?php

if(!defined('DS') ){
    define('DS', DIRECTORY_SEPARATOR);
}

define('GEQ_FUNCTIONS_DIR', dirname(__FILE__).DS."functions");

/**
 * Theme Support
 */
include_once( GEQ_FUNCTIONS_DIR.DS."theme-support.php");